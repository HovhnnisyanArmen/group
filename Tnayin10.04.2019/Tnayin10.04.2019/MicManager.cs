﻿using System;
using System.Collections.Generic;

namespace Tnayin10._04._2019
{
   public static class MicManager
    {
        public static List<Group> CreateGroups(List<People> teachers, List<People> students)
        {
            int count = teachers.Count;
            int[] countSt = new int[count];
            int temp = count;
            int temp1 = students.Count;
            for (int i = 0; i < count; i++)
            {
                countSt[i] = temp1 / temp;
                temp1 -= countSt[i];
                temp--;
            }
            string[] data = { "27.03.2019", "06.07.2019", "15.04.2019" };
            string[] groupNames = { "Fundamental", "OOP", "ASP.NET MVC" };
            List<Group> groups = new List<Group>();
            for (int i = 0; i < teachers.Count; i++)
            {
                groups.Add(new Group());
                groups[i].Teacher = (Teacher)teachers[i];
                groups[i]._groupName = "Mic Lesson "+groupNames[i];
                groups[i].Data = data[i];
                groups[i].Students = new List<Student>();
                for (int j = 0; j < countSt[i]; j++)
                {
                    
                    groups[i].Students.Add((Student)students[j]);
                    groups[i].Students[j]._studentId = (byte)(j + 1);
                }
                students.RemoveRange(0, countSt[i]);
            }
        
            return groups;
        }
        public static void Shufle(List<People> people)
        {
            Random rnd = new Random();
            int n = people.Count;
            while (n > 1)
            {
                n--;
                int k = rnd.Next(n + 1);
                SwapPeoples(people, k, n);
            }
        }
        public static void SwapPeoples(List<People> people, int index1, int index2)
        {
            People temp = people[index1];
            people[index1] = people[index2];
            people[index2] = temp;
        }
    }
}
