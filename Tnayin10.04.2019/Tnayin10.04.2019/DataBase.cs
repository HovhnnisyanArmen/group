﻿using System;
using System.Collections.Generic;

namespace Tnayin10._04._2019
{
    static class DataBase
    {
        public static List<People> CreatePeoples(int count, People people)
        {
            List<People> peoples = new List<People>();
            Random rand = new Random(DateTime.Now.Millisecond);
            string[] email = { "gmail.com","mail.ru","yandex.ru","yahoo.com","bk.ru","list.ru" };
            for (int i = 0; i < count; i++)
            {

                if (people is Teacher)
                {
                    peoples.Add(new Teacher
                    {
                        Name = $"Teacher{i+1}",
                        Surname = $"Teacher{i+1}yan",
                        Email = $"Teacher{i+1}yan@" + email[rand.Next(0, 5)],
                        Age = (byte)rand.Next(25, 70)
                    });
                }
                else
                {
                    peoples.Add(new Student
                    {
                        Name = $"Student{i+1}",
                        Surname = $"Student{i+1}yan",
                        Email = $"Student{i+1}yan@" + email[rand.Next(0, 5)],
                        Age = (byte)rand.Next(18, 50)
                    });
                }


            }
            return peoples;
        }
    }
}
