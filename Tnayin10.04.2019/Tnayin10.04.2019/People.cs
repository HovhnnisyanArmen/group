﻿using System;

namespace Tnayin10._04._2019
{
    public abstract class People
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public virtual byte Age { get; set; }
        
        public string Email { get; set; }
        public string FullName
        {
            get
            {
                return $"{Name}\t{Surname}";
            }
        }
        public void PrintInformationPeople()
        {
            if (this is Teacher)
                Console.ForegroundColor = ConsoleColor.DarkYellow;
            else
                Console.ForegroundColor = ConsoleColor.DarkGreen;
            
            Console.Write($"{FullName}\t{Age}\t{Email}");
            Console.ResetColor();
            Console.WriteLine();
        }

    }
}
