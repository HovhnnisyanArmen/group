﻿using System;


namespace Tnayin10._04._2019
{
   public  class Student:People
    {
        public byte _studentId { get; set; }
        public override byte Age
        {
            get => base.Age;
            set
            {
                if (value > 70 || value < 18)
                    base.Age = 18;
                else
                    base.Age = value;
            }
        }
    }
}
