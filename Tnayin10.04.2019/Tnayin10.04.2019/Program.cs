﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tnayin10._04._2019
{
    class Program
    {
        static void PrintGroup(Group group)
        {
            Console.ForegroundColor = ConsoleColor.DarkRed;
            Console.Write($"Group Name: {group._groupName}\tData: {group.Data}");
            Console.ResetColor();
            Console.Write("\n\nName\t\tSurName\t\tAge\tEmail\n\nTeacher\n");
            group.Teacher.PrintInformationPeople();
            Console.WriteLine("\nStudents");
            for (int i = 0; i < group.Students.Count; i++)
            {
                Console.Write($"{group.Students[i]._studentId}.");
                group.Students[i].PrintInformationPeople();
            }
        }
        static void Main(string[] args)
        {

            List<People> teachers = DataBase.CreatePeoples(3, new Teacher());
            List<People> students = DataBase.CreatePeoples(50, new Student());
            MicManager.Shufle(teachers);
            MicManager.Shufle(students);
            List<Group> groups = MicManager.CreateGroups(teachers, students);
            for (int i = 0; i < groups.Count; i++)
            {
                PrintGroup(groups[i]);
                Console.WriteLine();
            }
            
            Console.ReadLine();
        }
    }
}
