﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tnayin10._04._2019
{
    public class Teacher:People
    {

        
        public override byte Age
        {
            get => base.Age;
            set
            {
                if (value < 25 || value>70)
                    base.Age = 25;
                else
                    base.Age = value;
            }
        }
    }
}
