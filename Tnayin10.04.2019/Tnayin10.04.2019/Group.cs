﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tnayin10._04._2019
{
    public class Group
    {
        public string Data { get; set; }
        public string _groupName { get; set; }
        public List<Student> Students { get; set; }
        public Teacher Teacher { get; set; }

    }
}
